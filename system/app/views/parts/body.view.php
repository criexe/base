<?= _css(SYS_ASSETS . '/cx/css/style.css') ?>
<?= _css(SYS_ASSETS . '/cx/plugins/font-awesome/css/font-awesome.min.css') ?>
<?= _css(SYS_ASSETS . '/cx/plugins/animate.css') ?>
<?= _css(SYS_ASSETS . '/cx/plugins/materialize/css/materialize.min.css') ?>

<?= _css(SYS_ASSETS . '/cx/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css') ?>
<?= _js(SYS_ASSETS  . '/cx/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') ?>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">